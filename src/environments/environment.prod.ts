export const environment = {
  production: true,
  pollingTimeMs: 30000,
  statusApiURL: "https://enigmatic-castle-36157.herokuapp.com/status",
  useMockData: true
};
