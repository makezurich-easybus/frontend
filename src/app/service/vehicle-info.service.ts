import {Injectable} from '@angular/core';
import {VehicleInfo} from "../vehicle-info";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {of} from "rxjs";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class VehicleInfoService {

  MOCK: VehicleInfo[] =
    [
      {
        id: "2",
        vehicleType: "tram",
        line: "13",
        coord: {
          lat: 47.372123,
          lon: 8.599974
        },
        timestamp: new Date(),
        numberOfPeople: 70,
        capacity: 95
      },
      {
        id: "3",
        vehicleType: "tram",
        line: "13",
        coord: {
          lat: 47.377123,
          lon: 8.555674
        },
        timestamp: new Date(),
        numberOfPeople: 70,
        capacity: 95
      },
      {
        id: "4",
        vehicleType: "bus",
        line: "13",
        coord: {
          lat: 47.377123,
          lon: 8.539674
        },
        timestamp: new Date(),
        numberOfPeople: 70,
        capacity: 95
      },
      {
        id: "5",
        vehicleType: "tram",
        line: "13",
        coord: {
          lat: 47.37714,
          lon: 8.52967
        },
        timestamp: new Date(),
        numberOfPeople: 25,
        capacity: 95
      },
      {
        id: "6",
        vehicleType: "tram",
        line: "80",
        coord: {
          lat: 47.377023,
          lon: 8.539674
        },
        timestamp: new Date(),
        numberOfPeople: 70,
        capacity: 70
      }
    ];


  constructor(private http: HttpClient) {
  }

  getLatestInfo(): Observable<VehicleInfo[]> {
    if (environment.useMockData) {
      return of(this.MOCK);
    } else {
      return this.http.get<VehicleInfo[]>(environment.statusApiURL);
    }
  }

}
