import {Component, OnInit} from '@angular/core';
import {VehicleInfoService} from "../service/vehicle-info.service";
import {VehicleInfo} from "../vehicle-info";
import {MatTableDataSource} from "@angular/material";
import {startWith, switchMap} from "rxjs/operators";
import {interval} from "rxjs/internal/observable/interval";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  data: VehicleInfo[];
  private dataSource: MatTableDataSource<VehicleInfo>;
  displayedColumns: string[] = ['id', 'type', 'line', 'location', 'congestion', 'capacity'];

  constructor(private service: VehicleInfoService) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<VehicleInfo>(this.data);

    interval(environment.pollingTimeMs)
      .pipe(
        startWith(0),
        switchMap(() => this.service.getLatestInfo())
      )
      .subscribe(res => this.data = res);

  }

}
