import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {MapComponent} from "./map/map.component";
import {ListComponent} from "./list/list.component";

const appRoutes: Routes = [
  {path: 'map', component: MapComponent},
  {path: 'list', component: ListComponent},
  {path: '**', redirectTo: '/map'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
