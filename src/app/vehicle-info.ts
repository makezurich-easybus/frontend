export interface VehicleInfo {
  id: string,
  vehicleType: string,
  line: string,
  coord: {
    lat: number,
    lon: number
  },
  timestamp: Date
  numberOfPeople: number,
  capacity: number
}
