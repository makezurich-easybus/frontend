import datetime
import requests
import string
import time

gps = [
  {
    "lat": 47.377715,
    "lon": 8.541538
  },
  {
    "lat": 47.375658,
    "lon": 8.537333
  },
  {
    "lat": 47.376475,
    "lon": 8.534739
  },
  {
    "lat": 47.372961,
    "lon": 8.532921
  },
  {
    "lat": 47.371915,
    "lon": 8.531955
  },
  {
    "lat": 47.372663,
    "lon": 8.529284
  },
  {
    "lat": 47.374850,
    "lon": 8.532610
  },
  {
    "lat": 47.375918,
    "lon": 8.533897
  }
]

while True:
  for c in gps:
    date = datetime.datetime.now().isoformat()
    obj = string.Template(
      "{\"id\": \"abcd\",\"numberOfPeople\": 25,\"capacity\": 45,\"vehicleType\": \"tram\",\"line\": 17,\"coord\": {\"lat\": $lat,\"lon\": $lon},\"timestamp\": \"$date\"}")
    obj = obj.substitute(lat=c['lat'], lon=c['lon'], date=date)

    headers = {"Content-Type": "application/json"}
    r = requests.post("https://enigmatic-castle-36157.herokuapp.com/update", data=obj, headers=headers)
    print(r.status_code)
    time.sleep(1)
