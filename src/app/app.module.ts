import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './main/app.component';
import {MatButtonModule, MatIconModule, MatTableModule, MatToolbarModule} from "@angular/material";
import {MapComponent} from './map/map.component';
import {AppRoutingModule} from "./app-routing.module";
import {ListComponent} from './list/list.component';
import {AgmCoreModule} from '@agm/core';
import {AgmJsMarkerClustererModule} from "@agm/js-marker-clusterer";
import {HttpClient, HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatToolbarModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCQZqu2i-AWq_GpCCViJpRdMk4oQwN_3yQ'
    }),
    AgmJsMarkerClustererModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule {
}
